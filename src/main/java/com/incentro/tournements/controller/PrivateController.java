package com.incentro.tournements.controller;

import com.incentro.tournements.model.Competitor;
import com.incentro.tournements.model.Result;
import com.incentro.tournements.model.Tournement;
import com.incentro.tournements.service.CompetitorService;
import com.incentro.tournements.service.ResultService;
import com.incentro.tournements.service.TournementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class PrivateController {

    @Autowired
    private CompetitorService competitorService;

    @Autowired
    private TournementService tournementService;

    @Autowired
    private ResultService resultService;

    @PostMapping(value = "/save-results", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public void saveResults(@RequestBody List<Result> results) {
        List<Result> auxResult = new ArrayList<>();
        for (Result result: results) {
            if (result.getTournementId() != 0 && result.getCompetitorId() != 0 && result.getPointId() != 0){
                auxResult.add(result);
            }
        }
        resultService.saveAll(results);
    }

    /* Tournement */

    @PostMapping(value = "/save-tournement", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public void saveTournement(@RequestBody Tournement tournement) {
        tournementService.save(tournement);
    }

    @DeleteMapping(value = "/delete-tournement")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public void deleteTournement(@RequestBody Tournement tournement) {
        tournementService.delete(tournement);
    }

    /* User */
    @PostMapping(value = "/save-competitor", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public void saveCompetitor(@RequestBody Competitor competitor) {
        competitorService.save(competitor);
    }

    @DeleteMapping(value = "/delete-competitor", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public void deleteCompetitor(@RequestBody Competitor competitor) {
        competitorService.delete(competitor);
    }
}