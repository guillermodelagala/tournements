package com.incentro.tournements.controller;

import com.incentro.tournements.message.request.LoginForm;
import com.incentro.tournements.message.request.RegisterForm;
import com.incentro.tournements.message.response.JwtResponse;
import com.incentro.tournements.message.response.ResponseMessage;
import com.incentro.tournements.model.*;
import com.incentro.tournements.repository.RoleRepository;
import com.incentro.tournements.repository.UserRepository;
import com.incentro.tournements.security.jwt.JwtProvider;
import com.incentro.tournements.service.CompetitorService;
import com.incentro.tournements.service.PointService;
import com.incentro.tournements.service.ResultService;
import com.incentro.tournements.service.TournementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/public")
public class PublicController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	private CompetitorService competitorService;

	@Autowired
	private TournementService tournementService;

	@Autowired
	private PointService pointService;

	@Autowired
	private ResultService resultService;

	@PostMapping("/login")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	@PostMapping(value = "/logout")
	public void logoutDo(HttpServletRequest request, HttpServletResponse response){
		HttpSession session= request.getSession(false);
		SecurityContextHolder.clearContext();
		if(session != null) {
			session.invalidate();
		}
		for(Cookie cookie : request.getCookies()) {
			cookie.setMaxAge(0);
		}
	}

	@PostMapping("/register")
	public ResponseEntity<ResponseMessage> registerUser(@Valid @RequestBody RegisterForm form) {
		if (userRepository.existsByUsername(form.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}
		if (!"admin".equals(form.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Only admin is allowed to register!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(form.getUsername(), encoder.encode(form.getPassword()));

		Set<String> strRoles = form.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			if ("admin".equals(role)) {
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);
			} else {
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});

		user.setRoles(roles);
		userRepository.save(user);

		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}

	@GetMapping(value = "/admins", produces = "application/json")
	@ResponseBody
	public ResponseEntity<List<User>> getAdmins() {
		return ResponseEntity.ok(userRepository.findAll());
	}

	@GetMapping(value = "/competitors", produces = "application/json")
	@ResponseBody
	public ResponseEntity<List<Competitor>> getUsers() {
		return ResponseEntity.ok(competitorService.getAll());
	}

	@GetMapping(value = "/tournements", produces = "application/json")
	@ResponseBody
	public ResponseEntity<List<Tournement>> getTournements() {
		return ResponseEntity.ok(tournementService.getAll());
	}

	@GetMapping(value = "/points", produces = "application/json")
	@ResponseBody
	public ResponseEntity<List<Point>> getPoints() {
		return ResponseEntity.ok(pointService.getAll());
	}

	@GetMapping(value = "/results", produces = "application/json")
	@ResponseBody
	public ResponseEntity<List<Result>> getResults() {
		return ResponseEntity.ok(resultService.getAll());
	}
}