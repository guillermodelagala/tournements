package com.incentro.tournements.repository;

import com.incentro.tournements.model.Tournement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournementsRepository extends CrudRepository<Tournement, Long> {


}
