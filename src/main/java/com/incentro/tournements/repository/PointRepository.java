package com.incentro.tournements.repository;

import com.incentro.tournements.model.Point;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointRepository extends CrudRepository<Point, Long> {


}
