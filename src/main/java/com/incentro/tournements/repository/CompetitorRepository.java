package com.incentro.tournements.repository;

import com.incentro.tournements.model.Competitor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetitorRepository extends CrudRepository<Competitor, Long> {


}
