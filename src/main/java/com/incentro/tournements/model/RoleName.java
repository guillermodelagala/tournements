package com.incentro.tournements.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}