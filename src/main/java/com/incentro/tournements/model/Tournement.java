package com.incentro.tournements.model;

import javax.persistence.*;

@Entity
@Table(name = "tournements", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class Tournement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    public Tournement() {
    }

    public Tournement(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
