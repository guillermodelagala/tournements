package com.incentro.tournements.model;

import javax.persistence.*;

@Entity
@Table(name = "results", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "tournement_id", "competitor_id"
        })
})
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "competitor_id")
    private int competitorId;

    @Column(name = "tournement_id")
    private int tournementId;

    @Column(name = "point_id")
    private int pointId;

    public Result() {
    }

    public Result(int competitorId, int tournementId, int pointId) {
        this.competitorId = competitorId;
        this.tournementId = tournementId;
        this.pointId = pointId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCompetitorId() {
        return competitorId;
    }

    public void setCompetitorId(int competitorId) {
        this.competitorId = competitorId;
    }

    public int getTournementId() {
        return tournementId;
    }

    public void setTournementId(int tournementId) {
        this.tournementId = tournementId;
    }

    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }
}
