package com.incentro.tournements.model;

import javax.persistence.*;

@Entity
@Table(name = "competitors", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class Competitor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "competitor_id")
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    public Competitor() {
    }

    public Competitor(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
