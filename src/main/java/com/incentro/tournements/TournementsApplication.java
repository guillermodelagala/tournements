package com.incentro.tournements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TournementsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TournementsApplication.class, args);
    }

}

