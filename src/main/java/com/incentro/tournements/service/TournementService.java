package com.incentro.tournements.service;

import com.incentro.tournements.model.Tournement;

import java.util.List;

public interface TournementService {

    /* Get all tournements */
    List<Tournement> getAll();

    /* Save tournement */
    void save(Tournement tournement);

    /* Delete tournement */
    void delete(Tournement tournement);
}
