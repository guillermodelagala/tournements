package com.incentro.tournements.service;

import com.incentro.tournements.model.Point;

import java.util.List;

public interface PointService {

    /* Get all points */
    List<Point> getAll();
    /* ****************************** */

}
