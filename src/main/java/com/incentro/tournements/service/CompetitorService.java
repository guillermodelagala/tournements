package com.incentro.tournements.service;

import com.incentro.tournements.model.Competitor;

import java.util.List;

public interface CompetitorService {

    /* Get all competitors */
    List<Competitor> getAll();

    /* Save competitor */
    void save(Competitor competitor);

    /* Delete competitor */
    void delete(Competitor competitor);

}
