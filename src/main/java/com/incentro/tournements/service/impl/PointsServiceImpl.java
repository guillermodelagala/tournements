package com.incentro.tournements.service.impl;

import com.incentro.tournements.model.Point;
import com.incentro.tournements.repository.PointRepository;
import com.incentro.tournements.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PointsServiceImpl implements PointService {

    @Autowired
    private PointRepository pointRepository;

    public List<Point> getAll(){
        List<Point> points = new ArrayList<>();
        pointRepository.findAll().forEach(point -> points.add(point));
        return points;
    }
}
