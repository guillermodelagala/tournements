package com.incentro.tournements.service.impl;

import com.incentro.tournements.model.Result;
import com.incentro.tournements.repository.ResultRepository;
import com.incentro.tournements.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    @Autowired
    private ResultRepository resultRepository;

    public List<Result> getAll(){
        List<Result> results = new ArrayList<>();
        resultRepository.findAll().forEach(result -> results.add(result));
        return results;
    }

    public void save(Result result){
        resultRepository.save(result);
    }

    public void saveAll(List<Result> results){
        resultRepository.saveAll(results);
    }
}
