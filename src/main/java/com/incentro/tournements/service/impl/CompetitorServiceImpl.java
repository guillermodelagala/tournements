package com.incentro.tournements.service.impl;

import com.incentro.tournements.model.Competitor;
import com.incentro.tournements.repository.CompetitorRepository;
import com.incentro.tournements.service.CompetitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompetitorServiceImpl implements CompetitorService {

    @Autowired
    private CompetitorRepository competitorRepository;

    public List<Competitor> getAll(){
        List<Competitor> competitors = new ArrayList<>();
        competitorRepository.findAll().forEach(competitor -> competitors.add(competitor));
        return competitors;
    }

    public void save(Competitor competitor){
        competitorRepository.save(competitor);
    }

    public void delete(Competitor competitor){
        competitorRepository.delete(competitor);
    }
}
