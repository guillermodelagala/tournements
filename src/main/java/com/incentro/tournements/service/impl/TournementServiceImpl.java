package com.incentro.tournements.service.impl;

import com.incentro.tournements.model.Tournement;
import com.incentro.tournements.repository.TournementsRepository;
import com.incentro.tournements.service.TournementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TournementServiceImpl implements TournementService {

    @Autowired
    private TournementsRepository tournementRepository;

    public List<Tournement> getAll(){
        List<Tournement> tournements = new ArrayList<>();
        tournementRepository.findAll().forEach(tournement -> tournements.add(tournement));
        return tournements;
    }

    public void save(Tournement tournement){
        tournementRepository.save(tournement);
    }

    public void delete(Tournement tournement){
        tournementRepository.delete(tournement);
    }
}
