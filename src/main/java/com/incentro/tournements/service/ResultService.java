package com.incentro.tournements.service;

import com.incentro.tournements.model.Result;

import java.util.List;

public interface ResultService {

    /* Get all results */
    List<Result> getAll();

    /* Save all results */
    void saveAll(List<Result> results);

    /* Save result */
    void save(Result result);

}
